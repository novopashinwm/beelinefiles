﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace BeelineFiles
{
    class Program
    {
        /*-	Реализовать приложение на C#, которое будет выдавать список из 
          10-ти самых часто используемых слов, длина которых превышает заданную. 
          Слова должны подсчитываться в множестве текстовых файлов в указанной папке. 
          Приложение должно производить подсчёт в многопоточном режиме. 
      -	P.s. Ожидается реализация на Thread, для анализа слов использовать Regex
       */
        static volatile int CountWords;
        static int WordLength=0;
        static void Main(string[] args)
        {
            do {
                Console.Write("Введите минимальное количество символов в слове:");
                int.TryParse(Console.ReadLine(), out WordLength);
                Console.WriteLine();
            } while(WordLength == 0);

            Dictionary<string, string[]> files_words = GetFilesInfo();

            var threadList = new List<Thread>();
            foreach (var item in files_words.Values)
            {
                var t = new Thread(() => Handler.Handle(item));
                threadList.Add(t);
                t.Start();
            }

            WaitAll(threadList);

            var res = Handler.GetResult().OrderByDescending(x => x.Count).Take(10);
            foreach (var item in res)
            {
                Console.WriteLine($"The word is '{item.Name}' is '{item.Count}' counts.");
            }
        }

        public static void WaitAll(IEnumerable<Thread> threads)
        {
            if (threads != null)
            {
                foreach (Thread thread in threads)
                { thread.Join(); }
            }
        }

        public static class Handler
        {
            private static List<Metric> metrics = new List<Metric>();
            private static object locker = new Object();

            public static void Handle(string[] lines)
            {
                foreach (var text in lines)
                {
                    var result = GetUniqueWordsCount(text);
                    lock (locker)
                    {
                        foreach (var item in result)
                        {
                            var metric = metrics.FirstOrDefault(x => x.Name == item.Name);
                            if (metric == null)
                            {
                                metrics.Add(item);
                            }
                            else
                            {
                                metric.Count += item.Count;
                            }
                        }
                    }
                }
            }

            public static List<Metric> GetResult()
            {
                return metrics;
            }
        }

        public static List<Metric> GetUniqueWordsCount(string txt)
        {
            // Use regular expressions to replace characters
            // that are not letters or numbers with spaces.
            txt = new Regex("[^a-zA-Z0-9]").Replace(txt, " ");

            // Split the text into words.
            var words = txt.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // Use LINQ to get the unique words.
            var wordCount = words.GroupBy(x => x)
              .Where(x=> x.Key.Length> WordLength)
              .Select(n => new Metric { Name = n.Key,  Count = n.Count()})
              .ToList();

            return wordCount;

            //If you want words

        }

        public class Metric
        {
            public string Name { get; set; }
            public int Count { get; set; }
        }

        private static Dictionary<string, string[]> GetFilesInfo()
        {
            Dictionary<string, string[]> ret = new Dictionary<string, string[]>();
            foreach (var file in  Directory.GetFiles("files\\", "file*.txt"))            
                ret.Add(file, File.ReadAllLines(file));            
            return ret;
        }
    }
}
